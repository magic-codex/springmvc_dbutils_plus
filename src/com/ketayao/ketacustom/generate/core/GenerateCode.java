package com.ketayao.ketacustom.generate.core;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.ketayao.ketacustom.generate.util.FreeMarkers;
import com.ketayao.ketacustom.generate.util.Resources;
import com.ketayao.ketacustom.generate.vo.Column;
import com.ketayao.ketacustom.generate.vo.Table;

import freemarker.template.Template;

public class GenerateCode extends AbstractGenerate implements Generate
{
    
    public GenerateCode()
    {
        super();
    }
    
    @Override
    public void generate(Table table)
    {
        try
        {
            // 特殊类型处理
            handleSpecial(table.getColumns());
            model.put("tableName", table.getTableName().toLowerCase());
            model.put("columns", table.getColumns());
            model.put("pk", table.getPk());
            model.put("date", new Date());
            
            Map<String, String> map = new HashMap<String, String>();
            String className = (String)model.get("className");
            if ("jdbc".equals(Resources.TPL_FILE_DIR))
            {
                // ftl映射java类
                map.put("common_DataException.ftl", "DataException.java");
                map.put("common_ExceptionHandler.ftl", "ExceptionHandler.java");
                map.put("common_PaginationSupport.ftl", "PaginationSupport.java");
                map.put("core_BaseDAO.ftl", "BaseDAO.java");
                map.put("dao_dao.ftl", className + "DAO.java");
                map.put("dao_impl_daoImpl.ftl", className + "DAOImpl.java");
                map.put("entity_entity.ftl", className + ".java");
                map.put("service_service.ftl", className + "Service.java");
                map.put("service_impl_serviceImpl.ftl", className + "ServiceImpl.java");
                map.put("controller_controller.ftl", className + "Controller.java");
                map.put("test_ServiceTest.ftl", className + "ServiceTest.java");
                
            }
            for (String key : map.keySet())
            {
                // 将包名映射为路径、ftl中_转换为类路径
                String dirPath =
                    new File(javaPath + Resources.TPL_PACKAGE_NAME.replaceAll("\\.", Matcher.quoteReplacement(File.separator))) + separator
                        + StringUtils.substringBeforeLast(key, "_").replace("_", separator) + separator;
                String filePath = dirPath + map.get(key);
                File file = new File(filePath);
                // 文件不存在或者文件为1分钟之前生成的(保证此次生成的公共类文件不会重复生成)
                if (!file.exists() || System.currentTimeMillis() - file.lastModified() > 60 * 1000L)
                {
                    Template template = config.getTemplate(key);
                    String content = FreeMarkers.renderTemplate(template, model);
                    FileUtils.writeStringToFile(file, content, "UTF-8");
                    logger.info("filePath: {}", filePath);
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }
    
    @Override
    public void generate(List<Table> tables)
        throws Exception
    {
    }
    
    /**
     * 特殊类型处理
     * 
     * @param columns
     */
    private void handleSpecial(List<Column> columns)
    {
        boolean hasDate = false;
        boolean hasBigDecimal = false;
        for (Column column : columns)
        {
            if (column.getJavaType().equals("Date"))
            {
                hasDate = true;
            }
            else if (column.getJavaType().equals("BigDecimal"))
            {
                hasBigDecimal = true;
            }
        }
        model.put("hasDate", hasDate);
        model.put("hasBigDecimal", hasBigDecimal);
    }
}
