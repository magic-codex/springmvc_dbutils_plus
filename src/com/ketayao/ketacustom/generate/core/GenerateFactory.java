package com.ketayao.ketacustom.generate.core;

import com.ketayao.ketacustom.generate.db.Mysql;
import com.ketayao.ketacustom.generate.util.ConvertHandler;
import com.ketayao.ketacustom.generate.util.Resources;
import com.ketayao.ketacustom.generate.vo.Table;

public class GenerateFactory
{
    private Table table;
    
    private String tableName;
    
    public GenerateFactory()
    {
        this.tableName = Resources.TPL_TABLE_NAME;
    }
    
    /**
     * 
     */
    public GenerateFactory(String tableName)
    {
        this.tableName = tableName;
    }
    
    public GenerateFactory(Table table)
    {
        this.table = table;
    }
    
    private void init()
        throws Exception
    {
        Mysql db = new Mysql();
        table = db.getTable(tableName);
        ConvertHandler.tableHandle(table);
    }
    
    public void genJavaTemplate()
        throws Exception
    {
        if (table == null)
        {
            init();
        }
        new GenerateCode().generate(table);
    }
}
