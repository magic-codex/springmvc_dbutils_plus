package com.ketayao.ketacustom.generate.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ketayao.ketacustom.generate.util.Resources;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

/**
 * 数据库表信息工具
 * 
 * @author 00fly
 * @version [版本号, 2016-11-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class DataSource
{
    Logger log = LoggerFactory.getLogger(getClass());
    
    private static MysqlDataSource dataSource = new MysqlDataSource();
    
    // 使用ThreadLocal存储当前线程中的Connection对象
    private ThreadLocal<Connection> threadLocal = new ThreadLocal<Connection>();
    
    /**
     * 获取数据库连接
     * 
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     * @see [类、类#方法、类#成员]
     */
    protected Connection getConnection()
    {
        dataSource.setUrl(Resources.JDBC_URL);
        dataSource.setUser(Resources.JDBC_USERNAME);
        dataSource.setPassword(Resources.JDBC_PASSWORD);
        Connection connection = threadLocal.get();
        try
        {
            if (connection == null)
            {
                // 把 connection绑定到当前线程上
                connection = dataSource.getConnection();
                threadLocal.set(connection);
            }
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            throw new RuntimeException("Failed to get Mysql connection");
        }
        return connection;
    }
    
    /**
     * 查询数据库表字段名
     * 
     * @param tableName
     * @return
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public List<String> queryColumnNames(String tableName)
        throws SQLException
    {
        log.info("querySql: {}", tableName);
        Connection conn = null;
        List<String> columnNames = new ArrayList<String>();
        try
        {
            conn = getConnection();
            if (conn != null)
            {
                String sql = String.format("select * from %s", tableName);
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = null;
                rs = ps.executeQuery();
                ResultSetMetaData md = rs.getMetaData();
                int columnCount = md.getColumnCount();
                for (int i = 1; i <= columnCount; i++)
                {
                    columnNames.add(md.getColumnName(i));
                }
                ps.close();
                rs.close();
            }
        }
        catch (SQLException e)
        {
            log.error("--------- QuerySql--" + e.getMessage());
        }
        finally
        {
            if (conn != null && conn.getAutoCommit() == true)
            {
                freeConnection();
            }
            log.info("BaseDAOImpl querySql end ");
        }
        return columnNames;
    }
    
    /**
     * 释放数据库连接
     * 
     * @see [类、类#方法、类#成员]
     */
    public void freeConnection()
    {
        log.info("------释放数据库连接-------");
        try
        {
            Connection conn = threadLocal.get();
            if (conn != null)
            {
                conn.close();
                threadLocal.remove(); // 解除当前线程上绑定conn
            }
        }
        catch (Exception e)
        {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 测试
     * 
     * @param args
     * @throws SQLException
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
        throws SQLException
    {
        DataSource util = new DataSource();
        List<String> columns = util.queryColumnNames("sc_order");
        System.out.println(columns);
    }
}
