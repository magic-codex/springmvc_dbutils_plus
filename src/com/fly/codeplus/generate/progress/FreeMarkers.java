package com.fly.codeplus.generate.progress;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 
 * FreeMarkers
 * 
 * @author 00fly
 * @version [版本号, 2017-4-4]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class FreeMarkers
{
    /**
     * 模板配置
     * 
     * @param directory
     * @return
     * @throws IOException
     * @see [类、类#方法、类#成员]
     */
    public static Configuration buildConfiguration(String directory)
    {
        Configuration cfg = new Configuration();
        cfg.setTemplateLoader(new ClassTemplateLoader(ClassLoader.class, "/template"));
        cfg.setDefaultEncoding("UTF-8");
        return cfg;
    }
    
    /**
     * 获取模板填充model解析后的内容
     * 
     * @param template
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String renderTemplate(Template template, Object model)
    {
        try
        {
            StringWriter result = new StringWriter();
            template.process(model, result);
            return result.toString();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 获取模板填充model解析后的内容
     * 
     * @param model
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String renderTemplate(Map<String, Object> model)
    {
        try
        {
            Configuration cfg = FreeMarkers.buildConfiguration("classpath:/");
            Template template = cfg.getTemplate("config_ibator.xml.ftl");
            return FreeMarkers.renderTemplate(template, model);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 获取模板填充model解析后的内容
     * 
     * @param model
     * @param ftlName
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String renderTemplate(Map<String, Object> model, String ftlName)
    {
        try
        {
            Configuration cfg = FreeMarkers.buildConfiguration("classpath:/");
            Template template = cfg.getTemplate(ftlName);
            String result = FreeMarkers.renderTemplate(template, model);
            return result;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}